# Javelus Samples


Checkout `javelus-samples` into the same directory as `javelus`, `dpg`, and `developer-interface`.

1. Build `agent/`

        cd agent && ant


2. Build `tomcat-bms` or `h2-bms`

        cd tomcact-bms && ant


3. Run DaCapo Benchmark


        cd tomcat-bms && ./run.sh


4. Run JMeter and then open JMeter to run scripts under `tomcat-bms/jmeter`.


        cd tomcat-bms && ./standalone.sh
