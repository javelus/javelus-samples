#! /bin/bash
DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

DPG_HOME=${DIR}/../../dpg
DEVEL_HOME=${DIR}/../../developer-interface
DPG_BIN=${DPG_HOME}/dist/run.sh

BMS=h2
FROM=1.2.120
TO=1.2.121

NEW_CLASSPATH=
for JAR in ${DIR}/${BMS}-${TO}/*.jar
do
    NEW_CLASSPATH=$NEW_CLASSPATH:$JAR
done

for JAR in ${DIR}/lib/*.jar
do
    NEW_CLASSPATH=$NEW_CLASSPATH:$JAR
done

PATCH_DIR=${DIR}/${BMS}-${FROM}-${TO}

echo $NEW_CLASSPATH

ant -f ${DIR}/../samples-common.xml -Dnew.classpath=$NEW_CLASSPATH -Dpatch.dir=$PATCH_DIR compile-transformer
