#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

BMS=h2
RUN_CONF=$DIR/run.conf

source $RUN_CONF $BMS


if [ "x$BMS" == "x" ]; then
    echo "No Benchmark to run"
    exit -1
fi


DSU_SPEC_FILE="-d ${DIR}/${BMS}-${FROM}-${TO}/javelus.dsu"
#DSU_SPEC_FILE=



echo ===========================================
echo        Run benchmark ${BMS}
echo ===========================================
echo   JAVA: $JAVA
echo   VM_OPTS: $VM_OPTS
echo   ITERATION: $ITERATION
echo   DSU_SPEC_FILE: $DSU_SPEC_FILE
echo   COMMON_LIB: $COMMON_LIB
echo   BMS_LIB: $BMS_LIB
echo   CLASSPATH: $LOCALCLASSPATH
echo ===========================================

$JAVA $VM_OPTS -cp $LOCALCLASSPATH org.dacapo.harness.TestHarness -n $ITERATION $DSU_SPEC_FILE  $BMS
