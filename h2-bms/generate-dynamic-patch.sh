#! /bin/bash
DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

DPG_HOME=${DIR}/../../dpg
DEVEL_HOME=${DIR}/../../developer-interface
DPG_BIN=${DPG_HOME}/dist/run.sh

BMS=h2
FROM=1.2.120
TO=1.2.121

OLD_CLASSPATH=
for JAR in ${DIR}/${BMS}-$FROM/*.jar
do
    OLD_CLASSPATH=$OLD_CLASSPATH:$JAR
done


NEW_CLASSPATH=
for JAR in ${DIR}/${BMS}-${TO}/*.jar
do
    NEW_CLASSPATH=$NEW_CLASSPATH:$JAR
done

PATCH_DIR=${DIR}/${BMS}-${FROM}-${TO}
rm -rf $PATCH_DIR
mkdir -p $PATCH_DIR

ant -f ${DIR}/../samples-common.xml -Dold.classpath=$OLD_CLASSPATH -Dnew.classpath=$NEW_CLASSPATH -Doutput=$PATCH_DIR  generate-dynamic-patch
