#! /bin/bash

DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

RUN_CONF=${DIR}/run.conf

source $RUN_CONF h2

JAVELUS_AGENT_JAR=${DIR}/../agent/javelus-agent.jar
VM_OPTS="-javaagent:$JAVELUS_AGENT_JAR $VM_OPTS"

CATALINA_HOME=${DIR}/scratch

echo ===========================================
echo   Run tomcat standalone
echo ===========================================
echo   JAVA: $JAVA
echo   VM_OPTS: $VM_OPTS
echo   ITERATION: $ITERATION
echo   COMMON_LIB: $COMMON_LIB
echo   BMS_LIB: $BMS_LIB
echo   CLASSPATH: $LOCALCLASSPATH
echo ===========================================

$JAVA $VM_OPTS -cp $LOCALCLASSPATH org.h2.tools.Console
