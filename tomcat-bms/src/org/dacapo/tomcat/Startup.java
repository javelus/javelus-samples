package org.dacapo.tomcat;
import java.io.File;

import org.apache.catalina.startup.Tomcat;

public class Startup {
	
	static Tomcat daemon = null;
	public static void main(String[] args){
		
		//System.setProperty("catalina.home","E:/projects/samples/tomcat/TOMCAT_7_0_0/output/dist");
		//System.setProperty("catalina.home", "E:/projects/samples/tomcat/tomcat-bms/bin/./scratch");
		
		String catalinaHome = System.getProperty("catalina.home");
		if(catalinaHome == null){
			System.err.println("Catalina home is null. ");
			System.exit(1);
		}
		File catalinaBase = new File(catalinaHome);
		File rootBase = new File(catalinaBase, "/webapps/ROOT");
		File docBase = new File(catalinaBase, "/webapps/examples");
		File managerBase = new File(catalinaBase, "/webapps/manager");
		//File invokeBase = new File(catalinaBase, "/webapps/invokeDSU");
		if(daemon == null){
			Tomcat tomcat = new Tomcat();
			try{
				tomcat.setPort(8080);
				//tomcat.addWebapp(null,"", docBase.getAbsolutePath());
				tomcat.addWebapp(null,"", rootBase.getAbsolutePath());
				tomcat.addWebapp(null,"/examples", docBase.getAbsolutePath());
				tomcat.addWebapp(null,"/manager", managerBase.getAbsolutePath());
				//tomcat.addWebapp(null,"/invokeDSU", invokeBase.getAbsolutePath());
				tomcat.addUser("tomcat", "s3cret");
				tomcat.addRole("tomcat", "manager");
				
			}catch (Throwable t){
				t.printStackTrace();
				return;
			}
			daemon = tomcat;
		}
		

		
        try {
            String command = "start";
            if (args.length > 0) {
                command = args[args.length - 1];
            }
            
            if (command.equals("startd")) {
                daemon.start();
            } else if (command.equals("stopd")) {
                daemon.stop();
            } else if (command.equals("start")) {
                daemon.start();
                daemon.getServer().await();
            } else if (command.equals("stop")) {
                daemon.stop();
            } else {
                System.err.println("Bootstrap: command \"" + command + "\" does not exist.");
            }
        } catch (Throwable t) {
            t.printStackTrace();
        }
		
	}
}
