import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class JMeterParser {
	
	public static class HttpSample{
		String url;
		String time;
		String latency;
		String timeStamp;
		boolean succeed;
		String label;
		String threadName;
		String responceCode;
//		t="3" lt="3" ts="1334503257796" s="true" lb="HTTP请求" rc="200" tn="线程组 1-4" dt="text" by="2384"
	}
	

	
	public static void main(String[]args) throws Throwable{
		//String filePath = args[0];
		String filePath = "E:/projects/samples/tomcat/tomcat-bms/tomcat-simple-table.xml";
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();		
		Document document = null;
		if(filePath != null){
			FileInputStream fis = new FileInputStream(filePath);
			document = builder.parse(fis);//VM.DsuSpecificationFile);
		}
		
		if(document != null){
			Element testElement = document.getDocumentElement();
			Map<String,Map<String,List<HttpSample>>> results = new HashMap<String,Map<String,List<HttpSample>>>();
			List<Element> childElements = getChildElementsByLocalName(testElement,"httpSample");
			for(Element e: childElements){
				HttpSample sample = new HttpSample();
				sample.threadName = e.getAttribute("tn");
				sample.latency    = e.getAttribute("lt");
				sample.timeStamp  = e.getAttribute("ts");
				sample.responceCode = e.getAttribute("rc");
				Element urlElement = getChildElementByLocalName(e,"java.net.URL");
				sample.url = urlElement.getTextContent();
				Map<String,List<HttpSample>> result = results.get(sample.url);
				if(result==null){
					result = new HashMap<String,List<HttpSample>>();
					results.put(sample.url, result);
				}
				List<HttpSample> samples = result.get(sample.threadName); 
				if(samples == null){
					samples = new ArrayList<HttpSample>(300);
					result.put(sample.threadName, samples);
				}
				
				samples.add(sample);
			}
			
			BufferedWriter writer = new BufferedWriter(new FileWriter("results.txt"));
			for(Entry<String,Map<String,List<HttpSample>>> e : results.entrySet()){
				String url = e.getKey();
				Map<String,List<HttpSample>> result = e.getValue();
				for(Entry<String,List<HttpSample>> ee: result.entrySet()){
					String threadName = ee.getKey();
					List<HttpSample> samples = ee.getValue();
					for(HttpSample s : samples){
						String lt = s.latency;
						String rc = s.responceCode;
						String timeStamp = s.timeStamp;
						Date time = new Date(Long.valueOf(timeStamp));
						writer.write(url);
						writer.write(",");
						writer.write(threadName);
						writer.write(",");
						writer.write(lt);
						writer.write(",");
						writer.write(rc);
						writer.write(",");
						writer.write(timeStamp);
						writer.write("\n");
					}
				}
			}
			writer.close();
		}
		
	}
	
	static Element getChildElementByLocalName(Element parentElement, String nodeName) {
		NodeList children = parentElement.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (nodeName.equals(node.getNodeName()) ) {
				return (Element) node;
			}
		}
		return null;
	}
	
	static List<Element> getChildElementsByLocalName(Element parentElement, String nodeName){
		List<Element> list = new LinkedList<Element>();
		NodeList children = parentElement.getChildNodes();
		for (int i = 0; i < children.getLength(); i++) {
			Node node = children.item(i);
			if (nodeName.equals(node.getNodeName()) ) {
				list.add((Element) node);
			}
		}
		return list;
	}
	
}
