#! /bin/bash
DIR=$(pushd $(dirname $BASH_SOURCE[0]) > /dev/null && pwd && popd > /dev/null)

DPG_HOME=${DIR}/../../dpg
DEVEL_HOME=${DIR}/../../developer-interface
DPG_BIN=${DPG_HOME}/dist/run.sh

BMS=tomcat
FROM=7.0.3
TO=7.0.4

PATCH_DIR=${DIR}/${BMS}-${FROM}-${TO}

ant -f ${DIR}/../samples-common.xml -Dpatch.dir=$PATCH_DIR  merge-transformer
